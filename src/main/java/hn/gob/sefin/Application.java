package hn.gob.sefin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
// Modalidad utilizando caml context
@SpringBootApplication
@ImportResource({"classpath:camel-context.xml"})
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
*/


@SpringBootApplication
public class Application {
    
    // Modalidad por medio de clases

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
